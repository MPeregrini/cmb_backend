var express = require('express');
var router = express.Router();

class Database {
    constructor() {
        this._name = "";
    }
    get_name() {
        return this._name;
    }
    set_name(name) {
        this._name = name;
    }
}
const db = new Database();


/* GET name page. */
router.get('/', function(req, res, next) {
    res.send(`Hello ${db.get_name()}`);
  });
  
  /* POST name page. */
  router.post('/', function(req, res, next) {
    db.set_name(req.body.name);
    res.send(`Name ${db.get_name()} implanted`);
  });
  
  module.exports = router;
  