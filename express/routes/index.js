var express = require('express');
var router = express.Router();
const os = require("os");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send(`Hello world from ${os.hostname()}`);
});

/* POST home page. */
router.post('/', function(req, res, next) {
  res.send(req.body);
});

module.exports = router;
