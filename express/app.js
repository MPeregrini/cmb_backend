var createError = require('http-errors');
var express = require('express');
var session = require("express-session");
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var LocalStrategy = require('passport-local');

var indexRouter = require('./routes/index');
var nameRouter = require("./routes/name");
// var loginRouter = require("./routes/login");
const passport = require('passport');


var app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: "12test",
  resave: true,
  saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRouter);
app.use("/name", nameRouter);


// hardcoded users, ideally the users should be stored in a database
var users = [{"id":1, "username":"admin", "password":"password"}];
 
// passport needs ability to serialize and unserialize users out of session
passport.serializeUser(function (user, done) {
    done(null, users[0].id);
});
passport.deserializeUser(function (id, done) {
    done(null, users[0]);
});
 
// passport local strategy for local-login, local refers to this app
passport.use('local-login', new LocalStrategy(
    function (username, password, done) {
        if (username === users[0].username && password === users[0].password) {
            return done(null, users[0]);
        } else {
            return done(null, false, {"message": "User not found."});
        }
    })
);

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  console.log(req);
  if (req.isAuthenticated())
      return next();

  res.sendStatus(401);
}

// api endpoints for login, content and logout
app.get("/login", isLoggedIn, function (req, res) {
  res.send(`Hello ${req.user.username}`);
});

app.post("/login", 
  passport.authenticate("local-login", { failureRedirect: "/login"}),
  function (req, res) {
      res.send("Congrats, you are authentified !");
});

module.exports = app;
