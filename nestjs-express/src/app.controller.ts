import { Body, Controller, Request, Get, Post, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { Message } from './message.dto';
import { User } from "./user.dto";

import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService
    ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  postHello(@Body() message: Message) {
    return this.appService.postHello(message);
  }



  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getHelloAuth(@Request() req): string {
    return `Hello ${req.user.username}`;
  }


  
  @Get("name")
  getName(): string {
    return this.appService.getName();
  }

  @Post("name")
  postName(@Body() name: User) {
    return this.appService.postName(name);
  }
}