import { Injectable } from '@nestjs/common';
import { Message } from './message.dto';
import { User } from "./user.dto";
const os = require("os");
import db from './db';

@Injectable()
export class AppService {
  getHello(): string {
    return `Hello World! from ${os.hostname()}`;
  }

  getHelloAuth(user): string {
    return `Hello ${user.username}`;
  }

  postHello(data: Message): Message {
    console.log(data);
    return data;
  }

  getName(): string {
    return `Hello ${db.get_name()}`;
  }

  postName(user: User): string {
    db.set_name(user.username);
    return `Name ${db.get_name()} saved !`;
  }
}
