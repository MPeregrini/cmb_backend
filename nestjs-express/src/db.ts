class Database {

    _name: string;

    constructor() {
        this._name = "";
    }

    get_name() {
        return this._name;
    }

    set_name(name) {
        this._name = name;
    }
}

const db = new Database();

export default db;